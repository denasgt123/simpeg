<html>
    <head>
        <meta charset="UTF-8">
        <title>Kepegawaian SMKN 1 Surabaya</title>
        <style>
			@import url('https://font.googleapis.com/css2family-Poppins:wght@700&display=swap');
			*
			{
				margin: 0;
				padding: 0;
				box-sizing: border-box;
				font-family: 'Poppins', sans-serif;
			}
			body
			{
				display: flex;
				justify-content: center;
				align-items: center;
				min-height: 100vh;
				background: #242839;
			}
			.wrapper
			{
				display: inline-flex;
			}
			.wrapper .static-txt
			{
				color: #fff;
				font-size: 60px;
				font-weight: 400;
			}
			.wrapper .dynamic-txt
			{
				margin-left: 15px;
				overflow: hidden;
			}
			.dynamic-txt li
			{
				color: red;
				list-style: none;
				font-size: 60px;
				font-weight: 500;
			}
			.dynamic-txt li span
			{
				position: relative;
			}
			.dynamic-txt li span::after
			{
				content: "";
				position: absolute;
				left: 0;
				height: 100%;
				width: 100%;
				background: #242839;
				border-left: 4px solid red;
				animation: typing 3s steps(13) infinite;
			}
			@keyframes typing{
				100%{
					left: 100%;
					margin: 0 -35px 0 35px;
				}
			}
			ul
			{
				position: relative;
				display: flex;
				flex-direction: column;
			}
			ul li
			{
				position: relative;
				list-style: none;
			}
			ul li a
			{
				position: relative;
				left: 100px;
				font-size: 30px;
				text-decoration: none;
				letter-spacing: 2px;
				text-transform: uppercase;
				color: transparent;
				-webkit-text-stroke: 1px rgba(255, 255, 255, 0.5);
			}
			ul li a::before
			{
				content: attr(data-text);
				position: absolute;
				color:  var(--clr);
				width: 0;
				overflow: hidden;
				transition: 1s;
				bottom: 1px;
				border-right: 8px solid  var(--clr);
			}
			ul li a:hover::before
			{
				width: 100%;
				filter: drop-shadow(0 0 10px var(--clr));
				bottom: 1px;
			}
		</style>

    </head>
	
    <body>
        <div class="wrapper">
            <div class="static-txt">SIMPEG</div>
            <ul class="dynamic-txt">
                <li><span>SMKN 1 SURABAYA</span></li>
				<li style="--clr:#00ade1">
                <a data-text="&nbsp;Login" href="{{ route('login') }}">&nbsp;Login&nbsp;</a>
           		</li>
            </ul>
        </div>
    </body>
</html>