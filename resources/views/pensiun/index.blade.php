@extends('layouts.induk')
@section('konten')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                {{ $error }} <br/>
                @endforeach
            </div>
            @endif
        </div>
    </div>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-dark">Data Pensiun Pegawai</h6>
        </div>
        <div class="card-body">
            <a target="_blank" href="/pensiun/cetak_pensiun/" type="button" title="Pdf" class="btn btn-danger mb-3"><i class="fa fa-file-pdf"> Cetak Pensiun</i></a>
            <div class="table-responsive">
                <div class="row">
                    <div class="col-sm-12">
                        <table class="table table-bordered dataTable" id="dataTable" role="grid" aria-describedby="dataTable_info" style="width: 100%;" width="100%" cellspacing="0"> 
                            <thead>
                                <tr role="row">
                                    <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 162px;">NIP</th>
                                    <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 248px;">Nama</th>
                                    <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 100px;">Jabatan</th>
                                    <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 162px;">TTL</th>
                                    <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 116px;">Tgl Pensiun</th>
                                </tr>
                            </thead>
                
                            <tbody>
                                @foreach($pegawai as $p) 
                                <tr role="row" class="odd">
                                    <td>{{$p->nip}}</td>
                                    <td>{{$p->nama}}</td>
                                    <td>{{$p->sts_pegawai}}</td>
                                    <td>{{$p->t_lahir}}, {{date('d-m-Y', strtotime($p->tgl_lahir))}}</td>
                                    <td>{{date('d-m-Y', strtotime($p->tgl_pensiun))}}</td>
                                    @endforeach
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection