@extends('layouts.induk')
@section('konten')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                {{ $error }} <br/>
                @endforeach
            </div>
            @endif
        </div>
    </div>
    {{-- notifikasi sukses --}}
    @if ($sukses = $berkala)
    @foreach($sukses as $s)
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> 
        <strong>{{ $s->nama }}</strong> Akan Berkala Dalam 2 Bulan!!!
    </div>
    @endforeach
    @endif
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-dark">Data Berkala Pegawai</h6>
        </div>
        <div class="card-body">
            <a target="_blank" href="/berkala/cetak_berkala/" type="button" title="Pdf" class="btn btn-danger mb-3"><i class="fa fa-file-pdf"> Cetak Berkala</i></a>
        <div class="table-responsive">
        <div class="row">
                <div class="col-sm-12">
                    <table class="table table-bordered dataTable" id="dataTable" role="grid" aria-describedby="dataTable_info" style="width: 100%;" width="100%" cellspacing="0"> 
                    <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 248px;" >NIP</th>
                            <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 300px;" >Nama</th>
                            <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 200px;" >Mulai Berkala</th>
                            <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 200px;" >Berkala Selanjutnya</th>
                            <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 64px;" >Aksi</th>
                    </thead>
                    <tfoot>
                    </tfoot>
                    <tbody>
                    @foreach($berkalas as $p)   
                        <tr role="row" class="odd">
                        <td>{{$p->nip}}</td>
                        <td>{{$p->nama}}</td>
                        <td>{{date('d-m-Y', strtotime($p->berkala_sblm))}}</td>
                        <td>{{date('d-m-Y', strtotime($p->berkala_next))}}</td>
                        <td><button type="button" title="Tambah Berkala" class="btn btn btn-primary btn-sm" data-toggle="modal" data-target="#ModalEdit{{$p->id_berkala}}"><i class="fa fa-edit"></i></button></td>
                        </tr>

                         <!--  modal delete -->
                        <div class="modal fade" id="ModalEdit{{$p->id_berkala}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Konfirmasi</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                Anda yakin ingin menambah berkala??
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                                <a href="/berkala/edit/{{$p->id_berkala}}" class="btn btn-primary">Iya</a>
                            </div>
                            </div>
                        </div>
                        </div>
                        @endforeach
                    </tbody>
                </div>
            </div>
    </div>
</div>
</div>
</div>

@endsection