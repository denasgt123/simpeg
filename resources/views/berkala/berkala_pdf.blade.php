<head>
	<title>Membuat Laporan PDF Dengan DOMPDF Laravel</title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>
 
	<div class="container-fluid">
		<div class="row text-center">
			<h4>Data Pegawai Berkala Sampai 2 Bulan Mendatang</h4>
		</div>
		<br/>
		<table class="table">
			<thead class="thead-dark" style="left:1%;">
				<tr>
					<th scope="col">No</th>
					<th scope="col">Nama</th>
					<th scope="col">NIP</th>
					<th scope="col">Berkala Sekarang</th>
					<th scope="col">Berkala Selanjutnya</th>
				</tr>
			</thead>
			<tbody>
				@php $i=1 @endphp
				@foreach($berkalas as $p)
				<tr>
					<td>{{ $i++ }}</td>
					<td>{{$p->nama}}</td>
					<td>{{$p->nip}}</td>
					<td>{{date('d-m-Y', strtotime($p->berkala_sblm))}}</td>
					<td>{{date('d-m-Y', strtotime($p->berkala_next))}}</td>
				</tr>
				@endforeach
			</tbody>
	</table>
	</div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>