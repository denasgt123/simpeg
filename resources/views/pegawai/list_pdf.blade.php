<!DOCTYPE html>
<html>
<head>
	<title>Laporan Data Pegawai</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>

	<center>
		<h5>Data {{$pegawai->nama}}</h5>
	</center>
		
		
	<div class="card-body">
          <div class="row">
              <div class="col-md-4"> <pre>NIP                    : {{$pegawai->nip}}</pre></div>
              </div>
            

            <!-- row 2 -->

            <div class="row">
              <div class="col-md-4"> <pre>Nama                   : {{$pegawai->nama}}</pre></div>
            </div>
            

            <!-- row 3 -->

            <div class="row">
              <div class="col-md-4"> <pre>Tempat, Tanggal Lahir  : {{$pegawai->t_lahir}}, {{date('d-m-Y', strtotime($pegawai->tgl_lahir))}}</pre></div>
            </div>
            

            <!-- row 4 -->

            <div class="row">
              <div class="col-md-4"> <pre>Agama                  : {{ isset($pegawai->agama->agama) ? $pegawai->agama->agama : ''}}</pre></div>
            </div>
            

            <!-- row 5 -->

            <div class="row">
              <div class="col-md-4"> <pre>Jenis Kelamin          :@if($pegawai->jns_kelamin == 'L') Laki-laki @else Perempuan @endif</pre></div>
            </div>
            
    
            <!-- row 6 -->
            <div class="row">
              <div class="col-md-4"> <pre>Jabatan                : {{$pegawai->sts_pegawai}}</pre></div>
            </div>
            
    
            <!-- row 7 -->
            <div class="row">
              <div class="col-md-4"> <pre>Golongan               : {{isset($pegawai->golongan->golongan) ? $pegawai->golongan->golongan : ''}}</pre></div>
            </div>
            

            <!-- row 8 -->
            <div class="row">
              <div class="col-md-4"> <pre>Pangkat                : {{ isset($pegawai->golongan->pangkat) ? $pegawai->golongan->pangkat : ''}}</pre></div>
            </div>
            
            <!-- row 9 -->
            <div class="row">
              <div class="col-md-4"> <pre>TMT                    : {{isset($pegawai->tmt) ? date('d-m-Y', strtotime($pegawai->tmt)) : ''}}</pre></div>
            </div>

          <!-- row 9 -->
          <div class="row">
            <div class="col-md-4"> <pre>Tanggal Pensiun        : {{date('d-m-Y', strtotime($pegawai->tgl_pensiun))}}</pre></div>
          </div>

            <!-- row 10 -->
            <div class="row">
              <div class="col-md-4"> <pre>Sisa Jabatan           : {{$pegawai->sisa_jbtn}}</pre></div>
            </div>

            <!-- row 12 -->
            <div class="row">
              <div class="col-md-4"><pre>Pendidikan             : @foreach($pegawai->pendidikan as $pp){{$pp->pendidikan}}@endforeach {{$pegawai->nama_pendidikan}} ({{date('Y', strtotime($pegawai->tahun_sttb))}})</pre></div>
            </div>

            <!-- row 13 -->
            <div class="row">
              <div class="col-md-4"> <pre>Tanggal Mutasi         : {{$pegawai->tgl_mutasi}}</pre></div>
            </div>

            <!-- row 14 -->
            <div class="row">
              <div class="col-md-4"> <pre>Berkala Sekarang       : {{isset($berkalas->berkala_sblm) ? date('d-m-Y', strtotime($berkalas->berkala_sblm)) : ''}}</pre></div>
              
            </div>

            <!-- row 15 -->
            <div class="row">
              <div class="col-md-4"> <pre>Berkala Selanjutnya    : {{isset($berkalas->berkala_next) ? date('d-m-Y', strtotime($berkalas->berkala_next)) : ''}}</pre></div>
            </div>

            <!-- row 16 -->
            <div class="row">
              <div class="col-md-4"> <pre>Status Pernikahan      : {{$pegawai->sts_marital}}</pre></div>
            </div>
             
          </div>
        </div>
</body>
</html>