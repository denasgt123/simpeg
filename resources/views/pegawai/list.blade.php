@extends('layouts.induk')
@section('konten')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
    @if(count($errors) > 0)
				<div class="alert alert-danger">
					@foreach ($errors->all() as $error)
					{{ $error }} <br/>
					@endforeach
				</div>
				@endif
        
    </div>
  </div>
<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-dark">Data Pegawai</h6>
            </div>
            <div class="card-body">
              <button type="button" class="btn btn-danger mb-3" data-toggle="modal" data-target="#PDF">
                <i class="fa fa-file-pdf"> Cetak Golongan</i>
              </button>
              <div class="table-responsive">
                <div class="row"><div class="col-sm-12"><table class="table table-bordered dataTable" id="dataTable" role="grid" aria-describedby="dataTable_info" style="width: 100%;" width="100%" cellspacing="0"> 
                  <thead>
                    <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 116px;" aria-label="Position: activate to sort column ascending">NIP</th>
                    <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 200px;" aria-label="Office: activate to sort column ascending">Nama</th>
                    <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 200px;" aria-label="Office: activate to sort column ascending">Golongan dan Pangkat</th>
                    <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 116px;" aria-label="Office: activate to sort column ascending">TMT</th>
                    <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 116px;" aria-label="Office: activate to sort column ascending">Email</th>
                    <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 164px;" aria-label="Office: activate to sort column ascending">Aksi</th>
                    </thead>
                  <tfoot>
                  </tfoot>
                  <tbody>
                  @foreach($pegawai as $p)
                  <tr role="row" class="odd">
                      <td>{{$p->nip}}</td>
                      <td>{{$p->nama}}</td>
                      <td>{{isset($p->golongan->golongan) ? $p->golongan->golongan : ''}} {{isset($p->golongan->pangkat) ? '- ' . $p->golongan->pangkat : ''}}</td>
                      <td>{{isset($p->tmt) ? date('d-m-Y', strtotime($p->tmt)) : ''}}</td>
                      <td>{{$p->email}}</td>
                      <td><a class="btn btn-sm btn-secondary" title="Preview" href="/pegawai/profile/{{$p->id_peg}}"><i class="fa fa-list"></i></a> 
                      <a target="_blank" href="/pegawai/cetak_pdf/{{$p->id_peg}}" type="button" title="Pdf" class="btn btn-sm btn-danger"><i class="fa fa-file-pdf"></i></a>
                      <button type="button" title="Edit" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#ModalEdit{{$p->id_peg}}"><i class="fa fa-edit"></i></button>
                      <button type="button" title="Hapus" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#ModalDelete{{$p->id_peg}}"><i class="fa fa-trash"></i></button>
                     </td>
                  </tr>

                <!-- {{-- notifikasi form validasi --}} -->
                @if ($errors->has('file'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('file') }}</strong>
                </span>
                @endif

                <!-- modal edit -->
                <div class="modal fade" id="ModalEdit{{$p->id_peg}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                              <!-- form -->
                        @if(count($errors) > 0)
                        <div class="alert alert-danger">
                          @foreach ($errors->all() as $error)
                          {{ $error }} <br/>
                          @endforeach
                        </div>
                        @endif
                <div class="tab-content" id="myTabContent">
                  <div class="tab-pane fade show active" id="home{{$p->id_peg}}" role="tabpanel" aria-labelledby="home-tab{{$p->id_peg}}">
                  <form action="/pegawai/edit/{{$p->id_peg}}" method="post" enctype="multipart/form-data">
                  {{csrf_field()}}

                    <!-- Start tab 1 -->
                    <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="inputNip">NIP</label>
                      <input type="text" class="form-control" value="{{$p->nip}}" id="inputNip"  name="nip"  >
                    </div>
                    <div class="form-group col-md-6">
                      <label for="inputNama">Nama</label>
                      <input required type="text" class="form-control" value="{{$p->nama}}" id="inputNama"  name="nama" >
                    </div>
                  </div>
                  <div class="form-row">
                  <div class="form-group col-md-6">
                  <label for="inputTtl">Tempat</label>
                  <input type="text" name="t_lahir" value="{{$p->t_lahir}}" class="form-control">
                  </div>
                  <div class="form-group col-md-6" >
                  <label for="inputTgl">Tgl Lahir</label>
                  <input required type="date" name="tgl_lahir" value="{{$p->tgl_lahir}}" class="form-control">
                  </div>
                  </div>
                  <div class="form-row">
                <div class="form-group col-md-6">
                <label for="inputEmail">Email</label>
                      <input type="email" class="form-control" value="{{$p->email}}" id="inputEmail"  name="email" >
                    </div>
                    <div class="form-group col-md-6">
                  <label for="inputTgl">No.Telp</label>
                  <input type="text" id="inputTgl" name="no_telp" value="{{$p->no_telp}}" class="form-control">
                  </div>
                  </div>
                  <div class="form-row">
                  <div class="form-group col-md-4">
                  <label for="inputStatus">Status Pernikahan</label>
                  <select name="sts_pernikahan" id="inputUser" class="form-control" >
                   
                  <option value="Menikah" @if($p->sts_marital == 'Menikah') selected @endif >Menikah</option>
                  <option value="Belum menikah"  @if($p->sts_marital == 'Belum menikah') selected @endif >Belum menikah</option>
                  </select>
                  </div>

                  <div class="form-group col-md-8">
                  <label for="inputGolongan">Golongan dan Pangkat</label>
                  <select required name="gol" id="inputUser" class="form-control" >
                   
                  @foreach($gol as $gol2)
                  <option value="{{$gol2->kode_gol}}"@if($p->kode_gol == $gol2->kode_gol) selected @endif>{{$gol2->golongan}} - {{$gol2->pangkat}}</option>
                  @endforeach
                  </select>
                </div>
                  <!-- form row -->
                  </div>

                
                  
                  <!-- Tab 1 end -->

                  </div>
                  
                <div class="form-row">
                  <div class="form-group col-md-2">
                  <label for="inputStatus">Pendidikan</label>
                  <select require name="pendidikan" id="inputUser" class="form-control" >
                   
                  @foreach($pendidikan as $pend)
                  <option value="{{$pend->kode_pdd}}" @if($p->kode_pdd == $pend->kode_pdd) selected @endif >{{$pend->pendidikan}}</option>
                  @endforeach
                  </select>
                  </div>

                  <div class="form-group col-md-6">
                      <label for="inputNip">Nama Pendidikan</label>
                      <input type="text" class="form-control" id="inputNip"  value="{{$p->nama_pendidikan}}" name="namapendidikan"  >
                  </div>
                  <div class="form-group col-md-4">
                      <label for="inputNip">Tahun Lulus</label>
                      <input type="date" value="{{$p->tahun_sttb}}" name="sttb" id="inputKarpeg" class="form-control" >
                  </div>
                </div>

                <div class="form-row">
                <div class="form-group col-md-4">
                <label for="inputKarpeg">Tahun Berkala</label>
                <input type="date" value="{{$p->tahun_berkala}}" name="sttb" id="inputKarpeg" class="form-control" >
                </div>
                <div class="form-group col-md-4">
                <label for="inputKarsu">Gelar Depan</label>
                <input type="text" name="gelard" value="{{$p->gelar_depan}}"  id="inputKarsu" class="form-control" >
                </div>
                <div class="form-group col-md-4">
                  <label for="inputAskes">Gelar Belakang</label>
                  <input type="text" name="gelarb" id="inputAskes" value="{{$p->gelar_belakang}}" class="form-control" >
                </div>
                </div>

                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="inputKarpeg">Tanggal Mutasi</label>
                    <input type="date" value="{{$p->tgl_mutasi}}" name="mutasi" id="inputKarpeg" class="form-control" >
                  </div>
                  <div class="form-group col-md-6" >
                    <label for="inputStatus">Agama</label>
                    <select required name="agama" id="inputUser" class="form-control" >
                      <option></option>
                      @foreach($agama as $agm)
                      <option value="{{$agm->kode_agama}}" @if($p->kode_agama == $agm->kode_agama) selected @endif >{{$agm->agama}}</option>
                      @endforeach
                    </select>
                  </div>
                <!-- form row  end-->
                  
                  </div>
                </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Ubah</button></form>
                      </div>
                    </div>
                  </div>
                </div>

                <!--  modal delete -->

                <div class="modal fade" id="ModalDelete{{$p->id_peg}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Konfirmasi</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        Anda yakin ingin menghapus?
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                        <a href="/pegawai/hapus/{{$p->id_peg}}" class="btn btn-primary">Iya</a>
                      </div>
                    </div>
                  </div>
                </div>

                @endforeach

                </tbody>
                </table>
                </div>
                </div>




               <!-- Modal Tambah -->
               <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tambah Pegawai</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <!-- form -->

                      <form action="/pegawai/tambah/proses" method="POST" enctype="multipart/form-data">
                      {{ csrf_field() }}
                

                      <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                  <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Umum</a>
                  <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Detail 1</a>
                </div>
              </nav>
              <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <!-- Tab 1 -->

                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="inputNip">NIP</label>
                    <input type="text" class="form-control" id="inputNip"  name="nip"  >
                  </div>
                  <div class="form-group col-md-6">
                    <label for="inputNama">Nama</label>
                    <input required type="text" class="form-control" id="inputNama"  name="nama" >
                  </div>
                </div>
                <div class="form-row">
                <div class="form-group col-md-3">
                <label for="inputTtl">Tempat</label>
                <input type="text" name="t_lahir" class="form-control">
                </div>
                <div class="form-group col-md-4" >
                <label for="inputTgl">Tgl Lahir</label>
                <input required type="date" name="tgl_lahir" class="form-control">
                </div>
                <div class="form-group col-md-5">
                <label for="inputTgl">No.Telp</label>
                <input type="text" id="inputTgl" name="no_telp" class="form-control">
                </div>
                </div>
                <div class="form-row">
                <div class="form-group col-md-6">
                <label for="inputKelamin">Jenis Kelamin</label>
                <select required name="kelamin" id="inputKelamin" class="form-control">
                <option value="L">Laki-laki</option>
                <option value="P">Perempuan</option>
                </select>
                </div>

                <div class="form-group col-md-6" >
                <label for="inputStatus">Jabatan</label>
                <select required name="sts_pegawai" id="inputStatus" class="form-control" >
                 
                <option value="Guru">Guru</option>
                <option value="Pegawai">Pegawai</option>
                </select>
                </div>
                <!-- form row -->
                </div>

              <div class="form-row">
              <div class="form-group col-md-6">
                    <label for="inputNip">Tgl Mulai Berkala</label>
                    <input type="date" name="tgl_berkala" class="form-control">
                  </div>
                <div class="form-group col-md-6">
                <label for="inputFoto">Email</label>
                <input type="email" class="form-control" name="email">
                </div>
                </div>
                <!-- Tab 1 end -->
                
                </div>
                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                <!-- Tab 2 -->
                <div class="form-row">
                  <div class="form-group col-md-4">
                    <label for="inputNip">NIP Lama</label>
                    <input type="text" class="form-control" id="inputNip"  name="niplama"  >
                  </div>
                  <div class="form-group col-md-4">
                    <label for="inputNip">Nama Jabatan</label>
                    <select name="namajabatan" id="inputUser" class="form-control">
                      <option value=""></option>
                      <option value="Guru Madya">Guru Madya</option>
                      <option value="Guru Muda">Guru Muda</option>
                      <option value="Guru Pertama">Guru Pertama</option>
                    </select>
                  </div>
              <div class="form-group col-md-4" >
                <label for="inputStatus">Agama</label>
                <select name="agama" id="inputUser" class="form-control" required>
                 
                @foreach($agama as $agama)
                <option value="{{$agama->kode_agama}}">{{$agama->agama}}</option>
                @endforeach
                </select>
                </div>
                </div>
              <div class="form-row">
              <div class="form-group col-md-2" >
                <label for="inputStatus">Pendidikan</label>
                <select name="pendidikan" id="inputUser" class="form-control" required>
                 
                @foreach($pendidikan as $p)
                <option value="{{$p->kode_pdd}}">{{$p->pendidikan}}</option>
                @endforeach
                </select>
                </div>

                <div class="form-group col-md-6">
                  <label for="inputNip">Nama Pendidikan</label>
                  <input type="text" class="form-control" id="inputNip"  name="namapendidikan"  >
                </div>

                <div class="form-group col-md-4">
                  <label for="inputNip">Tahun Lulus</label>
                  <input type="date" name="sttb" id="inputKarpeg" class="form-control" >
                </div>
              </div>

              
              <div class="form-row">
                <div class="form-group col-md-4" >
                  <label for="inputStatus">Golongan</label>
                  <select name="gol" id="inputUser" class="form-control" required>
                  
                  @foreach($gol as $gol1)
                  <option value="{{$gol1->kode_gol}}">{{$gol1->golongan}} - {{$gol1->pangkat}}</option>
                  @endforeach
                  </select>
                </div>
                <div class="form-group col-md-4">
                      <label for="inputNip">TMT</label>
                      <input type="date" name="tgl_tmt" class="form-control">
                </div>
                  
                <div class="form-group col-md-4">
                  <label for="inputNip">Tgl Mutasi</label>
                  <input type="date" name="tgl_mutasi" class="form-control">
              </div>
            </div>
          </div>
        </div>
          
          <!-- Tab 2 end -->
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                      <button type="submit" class="btn btn-primary">Tambah</button></form>
                    </div>
                  </div>
                </div>
              </div>
              <div class="drop">
                <a class="float" href="#" title="Tambah Data Pegawai">
                  <i class="fa fa-plus my-float"></i>
                </a>
                <div class="drop-content">
                  <a data-toggle="modal" data-target="#exampleModal" href="#" title="Tambah Data Pegawai">Tambah Pegawai</a>
                  <a data-toggle="modal" data-target="#importExcel" href="#" title="Import Data Pegawai">Import Data Pegawai</a>
                  <a href="{{ asset('storage/Template.xlsx') }}" title="Import Data Pegawai"> Download Template Excel</a>
                </div>
              </div>
            </div>

               <!-- Import Excel -->
               <div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <form method="post" action="/pegawai/import_excel" enctype="multipart/form-data">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
                        </div>
                        <div class="modal-body">
            
                          {{ csrf_field() }}
            
                          <label>Pilih file excel</label>
                          <div class="form-group">
                            <input type="file" name="file" required="required">
                          </div>
            
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary">Import</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>

                <!-- PDF Golongan -->
                <div class="modal fade" id="PDF" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Cetak Pegawai Berdasarkan Golongan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                        
                        <form action="/pegawai/cetak_gol" method="POST" enctype="multipart/form-data">
                      </div>
                      <div class="modal-body">

                        {{ csrf_field() }}
                  
                        <div class="form-row">
                          <label for="inputStatus">Golongan</label>
                          <select name="gol" id="inputUser" class="form-control" required>
                          
                          @foreach($gol as $gol)
                          <option value="{{$gol->kode_gol}}">{{$gol->golongan}} - {{$gol->pangkat}}</option>
                          @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                        <button type="submit" class="btn btn-primary">Cetak</button></form>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      
      @endsection
      
