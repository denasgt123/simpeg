@extends('layouts.induk')
@section('konten')
<div class="container-fluid">
  <div class="row"> 
    <div class="col-md-12">
  @if(count($errors) > 0)
      <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
        {{ $error }} <br/>
        @endforeach
      </div>
				@endif
    </div>
  </div>
  @if ($message = Session::get('alert-success'))
  <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
      <strong>{{ $message }}</strong>
  </div>
    @endif
  <div class="row justify-content-center">
    <div class="col-9 md-12">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-dark text-center">{{$pegawai->nama}}</h6>
        </div>
        <div class="card-body">
          <div class="row">
              <div class="col-md-4"> NIP </div>
              <div class="col-md-8"> : {{$pegawai->nip}}</div>
              </div>
            <hr>

            <!-- row 2 -->

            <div class="row">
              <div class="col-md-4"> Nama </div>
              <div class="col-md-8"> : {{$pegawai->nama}}</div>
            </div>
            <hr>

            <!-- row 3 -->

            <div class="row">
              <div class="col-md-4"> Tempat, Tanggal Lahir</div>
              <div class="col-md-8"> : {{$pegawai->t_lahir}}, {{date('d-m-Y', strtotime($pegawai->tgl_lahir))}}</div>
            </div>
            <hr>

            <!-- row 4 -->

            <div class="row">
              <div class="col-md-4"> Agama</div>
              <div class="col-md-8"> : {{ isset($pegawai->agama->agama) ? $pegawai->agama->agama : ''}}</div>
            </div>
            <hr>

            <!-- row 5 -->

            <div class="row">
              <div class="col-md-4"> Jenis Kelamin</div>
              <div class="col-md-8"> : @if($pegawai->jns_kelamin == 'L') Laki-laki @else Perempuan @endif</div>
            </div>
            <hr>
    
            <!-- row 6 -->
            <div class="row">
              <div class="col-md-4"> Jabatan</div>
              <div class="col-md-8"> : {{$pegawai->sts_pegawai}}</div>
            </div>
            <hr>
    
            <!-- row 7 -->
            <div class="row">
              <div class="col-md-4"> Golongan</div>
              <div class="col-md-8"> : {{isset($pegawai->golongan->golongan) ? $pegawai->golongan->golongan : ''}}</div>
            </div>
            <hr>

            <!-- row 8 -->
            <div class="row">
              <div class="col-md-4"> Pangkat</div>
              <div class="col-md-8"> : {{ isset($pegawai->golongan->pangkat) ? $pegawai->golongan->pangkat : ''}}</div>
            </div>
            <hr>

            <div class="row">
              <div class="col-md-4"> TMT</div>
              <div class="col-md-8"> :{{isset($pegawai->tmt) ? date('d-m-Y', strtotime($pegawai->tmt)) : ''}}</div>
            </div>
            <hr>

            <!-- row 9 -->
            <div class="row">
              <div class="col-md-4"> Status Pernikahan</div>
              <div class="col-md-8"> : {{$pegawai->sts_marital}}</div>
            </div>
            <hr>

            <!-- row 10 -->
            <div class="row">
              <div class="col-md-4"> Pendidikan</div>
              <div class="col-md-8"> : @foreach($pegawai->pendidikan as $pp) <span class="badge badge-primary">{{$pp->pendidikan}}</span> @endforeach {{$pegawai->nama_pendidikan}} ({{date('Y', strtotime($pegawai->tahun_sttb))}})</div>
            </div>
            <hr>

            <!-- row 12 -->
            <div class="row">
              <div class="col-md-4"> Tanggal Mutasi</div>
              <div class="col-md-8"> : {{$pegawai->tgl_mutasi}}</div>
            </div>
            <hr> 
          </div>
        </div>
      </div>
    </div> 
  </div> 
@endsection