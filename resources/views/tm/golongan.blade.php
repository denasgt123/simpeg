@extends('layouts.induk')
@section('konten')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
    @if(count($errors) > 0)
				<div class="alert alert-danger">
					@foreach ($errors->all() as $error)
					{{ $error }} <br/>
					@endforeach
				</div>
				@endif
        
    </div>
  </div>
<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-dark">Data Master Golongan</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <div class="row"><div class="col-sm-12"><table class="table table-bordered dataTable" id="dataTable" role="grid" aria-describedby="dataTable_info" style="width: 100%;" width="100%" cellspacing="0"> 
                  <thead>
                    <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 116px;" aria-label="Position: activate to sort column ascending">No</th>
                    <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 200px;" aria-label="Office: activate to sort column ascending">Golongan</th>
                    <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 200px;" aria-label="Office: activate to sort column ascending">Pangkat</th>
                    <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 164px;" aria-label="Office: activate to sort column ascending">Aksi</th>
                    </thead>
                  <tfoot>
                  </tfoot>
                  <tbody>
                  @foreach($gol as $p)
                  <tr role="row" class="odd">
                      <td>{{$p->kode_gol}}</td>
                      <td>{{$p->golongan}}</td>
                      <td>{{$p->pangkat}}</td>
                      <td>
                      <button type="button" title="Hapus" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#ModalDelete{{$p->kode_gol}}"><i class="fa fa-trash"></i></button>
                    </td>
                  </tr>

                <!--  modal delete -->

                <div class="modal fade" id="ModalDelete{{$p->kode_gol}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Konfirmasi</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        Anda yakin ingin menghapus?
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                        <a href="/pegawai/tmgolongan/hapus/{{$p->kode_gol}}" class="btn btn-primary">Iya</a>
                      </div>
                    </div>
                  </div>
                </div>

                @endforeach

                </tbody>
                </table>
                </div>
                </div>




                <!-- Modal Tambah -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tambah Golongan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <form action="/pegawai/tmgolongan/tambah/proses" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                  <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                  <!-- Tab 1 -->

                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="inputNip">Golongan</label>
                      <input type="text" class="form-control" id="inputNip"  name="golongan"  >
                    </div>
                    <div class="form-group col-md-6">
                      <label for="inputNama">Pangkat</label>
                      <input type="text" class="form-control" id="inputNama"  name="pangkat" >
                    </div>
                </div>
              </div>
            </div>
            
            <!-- Tab 2 end -->
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                        <button type="submit" class="btn btn-primary">Tambah</button></form>
                      </div>
                    </div>
                  </div>
                </div>
                <?php    
                  if(Auth::user()->level == 'admin') { 
                ?>
                  <a class="float" a data-toggle="modal" data-target="#exampleModal" href="#" title="Tambah Data Master Golongan">
                    <i class="fa fa-plus my-float"></i>
                  </a>
                <?php
                }
                ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      @endsection
      
