@extends('layouts.induk')
@section('konten')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
    @if(count($errors) > 0)
				<div class="alert alert-danger">
					@foreach ($errors->all() as $error)
					{{ $error }} <br/>
					@endforeach
				</div>
				@endif
        
    </div>
  </div>
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-dark">Data User</h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <div class="row"><div class="col-sm-12"><table class="table table-bordered dataTable" id="dataTable" role="grid" aria-describedby="dataTable_info" style="width: 100%;" width="100%" cellspacing="0"> 
        <thead>
          <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 116px;" aria-label="Position: activate to sort column ascending">No</th>
          <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 200px;" aria-label="Office: activate to sort column ascending">Nama</th>
          <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 200px;" aria-label="Office: activate to sort column ascending">Level</th>
          <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 164px;" aria-label="Office: activate to sort column ascending">Aksi</th>
          </thead>
        <tfoot>
        </tfoot>
        <tbody>
        @foreach($user as $p)
        <tr role="row" class="odd">
            <td>{{$p->id}}</td>
            <td>{{$p->name}}</td>
            <td>{{$p->level}}</td>
            <td>
            <button type="button" title="Hapus" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#ModalDelete{{$p->id}}"><i class="fa fa-trash"></i></button>
            </td>
        </tr>

      <!--  modal delete -->

      <div class="modal fade" id="ModalDelete{{$p->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Konfirmasi</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              Anda yakin ingin menghapus?
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
              <a href="/user/hapus/{{$p->id}}" class="btn btn-primary">Iya</a>
            </div>
          </div>
        </div>
      </div>

      @endforeach

      </tbody>
      </table>
      </div>
      </div>




      <!-- Modal Tambah -->
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Tambah User</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="/user/tambah/proses" method="POST" enctype="multipart/form-data">
              {{ csrf_field() }}
        <!-- Tab 1 -->

        <div class="row mb-3">
            <label for="name" class="col-md-4 col-form-label text-md-end">Name : </label>
            <div class="col-md-6">
              <input id="name" type="text" class="form-control" name="name" required>
            </div>
          </div>

        <div class="row mb-3">
          <label for="email" class="col-md-4 col-form-label text-md-end">Email Address : </label>
          <div class="col-md-6">
              <input id="email" type="email" class="form-control" name="email" required>
          </div>
        </div>

        <div class="row mb-3">
          <label class="col-md-4 col-form-label text-md-end" for="inputLevel">Level : </label>
          <div class="col-md-6">
            <select name="level" id="inputLevel" class="form-control" >
             
            <option value="User">User</option>
            <option value="Admin">Admin</option>
            </select>
          </div>
        </div>

        <div class="row mb-3">
          <label for="password" class="col-md-4 col-form-label text-md-end">Password : </label>

          <div class="col-md-6">
              <input id="password" type="password" class="form-control" name="password" required>
          </div>
        </div>

        <div class="row mb-3">
            <label for="password-confirm" class="col-md-4 col-form-label text-md-end">Confirm Password : </label>
            <div class="col-md-6">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
          </div>
        </div>
      </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
              <button type="submit" class="btn btn-primary">Tambah</button></form>
            </div>
          </div>
        </div>
      </div>
      <?php    
        if(Auth::user()->level == 'admin') { 
      ?>
        <a class="float" a data-toggle="modal" data-target="#exampleModal" href="#" title="Tambah Data Master Agama">
          <i class="fa fa-plus my-float"></i>
        </a>
      <?php
      }
      ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      @endsection
      
