<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();


Route::group(['middleware' => 'auth'], function () {

    Route::get('/pegawai', [App\Http\Controllers\ListPegawaiController::class, 'beranda']);

    Route::get('/profile', function () {
        return view('pegawai.profile');
    });
    Route::get('/list', [App\Http\Controllers\ListPegawaiController::class, 'index']);
    Route::get('/pensiun', [App\Http\Controllers\PensiunController::class, 'index']);
    Route::get('/berkala', [App\Http\Controllers\BerkalaController::class, 'index']);
    
    Route::get('/user/tambah', [App\Http\Controllers\UserController::class, 'index']);
    Route::post('/user/tambah/proses', [App\Http\Controllers\UserController::class, 'tambah']);
    Route::get('/user/hapus/{id}', [App\Http\Controllers\UserController::class, 'hapus']);

    Route::get('/pegawai/tambah', [App\Http\Controllers\ListPegawaiController::class, 'tambah']);
    Route::post('/pegawai/tambah/proses', [App\Http\Controllers\ListPegawaiController::class, 'tambah']);
    Route::post('/pegawai/cetak_gol', [App\Http\Controllers\ListPegawaiController::class, 'cetak_gol']);
    Route::post('/pegawai/edit/{id}', [App\Http\Controllers\ListPegawaiController::class, 'edit']);
    Route::get('/pegawai/hapus/{id}', [App\Http\Controllers\ListPegawaiController::class, 'hapus']);
    Route::get('/pegawai/profile/{id}', [App\Http\Controllers\ListPegawaiController::class, 'profile']);
    Route::post('/pegawai/import_excel', [App\Http\Controllers\ListPegawaiController::class, 'import_excel']);
    Route::get('/pegawai/cetak_pdf/{id}', [App\Http\Controllers\ListPegawaiController::class, 'cetak_pdf']);

    Route::get('/pensiun/cetak_pensiun', [App\Http\Controllers\PensiunController::class, 'cetak_pensiun']);
    Route::get('/berkala/cetak_berkala', [App\Http\Controllers\BerkalaController::class, 'cetak_berkala']);

    Route::get('/berkala/edit/{id}', [App\Http\Controllers\BerkalaController::class, 'edit']);

    Route::get('/pegawai/tmagama/tambah', [App\Http\Controllers\TmAgamaController::class, 'index']);
    Route::post('/pegawai/tmagama/tambah/proses', [App\Http\Controllers\TmAgamaController::class, 'tambah']);
    Route::get('/pegawai/tmagama/hapus/{id}', [App\Http\Controllers\TmAgamaController::class, 'hapus']);

    Route::get('/pegawai/tmgolongan/tambah', [App\Http\Controllers\TmGolonganController::class, 'index']);
    Route::post('/pegawai/tmgolongan/tambah/proses', [App\Http\Controllers\TmGolonganController::class, 'tambah']);
    Route::get('/pegawai/tmgolongan/hapus/{id}', [App\Http\Controllers\TmGolonganController::class, 'hapus']);


    Route::get('/pegawai/tmpangkat/tambah', [App\Http\Controllers\TmPangkatController::class, 'index']);
    Route::post('/pegawai/tmpangkat/tambah/proses', [App\Http\Controllers\TmPangkatController::class, 'tambah']);
    Route::get('/pegawai/tmpangkat/hapus/{id}', [App\Http\Controllers\TmPangkatController::class, 'hapus']);


    Route::get('/pegawai/tmpendidikan/tambah', [App\Http\Controllers\TmPendidikanController::class, 'index']);
    Route::post('/pegawai/tmpendidikan/tambah/proses', [App\Http\Controllers\TmPendidikanController::class, 'tambah']);
    Route::get('/pegawai/tmpendidikan/hapus/{id}', [App\Http\Controllers\TmPendidikanController::class, 'hapus']);
});
