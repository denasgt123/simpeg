<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use DateTime;
use App\Models\ListPegawai;
use Illuminate\Support\Str;
use \App\Models\Berkala;
use \PhpOffice\PhpSpreadsheet\Shared\Date as DateExcel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ListPegawaiImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $peg = new ListPegawai;
        $peg->nip = $row['nip'];
        $peg->nama = $row['nama'];
        $peg->nip_lama = $row['nip_lama'];
        $peg->t_lahir = $row['t_lahir'];
        $peg->sts_pegawai = strval($row['sts_pegawai']);
        $peg->kode_gol = $row['kode_gol'];
        $peg->kode_pdd = $row['kode_pdd'];
        $peg->kode_agama = $row['kode_agama'];
        $peg->jns_kelamin = $row['jns_kelamin'];
        $peg->tahun_sttb = $row['tahun_sttb'];
        // $peg->tmt = DateExcel::excelToDateTimeObject($row['tmt']);
        $peg->nama_pendidikan = $row['nama_pendidikan'];

        $tglahir = DateExcel::excelToDateTimeObject($row['tgl_lahir']);
        $status = Str::lower($peg->sts_pegawai);
        if($status == 'pegawai') {
            $timeToAdd = "+ 58 years 1 months";
            // $objDateTime = DateTime::createFromFormat("Y-m-d",$tglahir);
            $objDateTime = $tglahir;
            $objDateTime->modify($timeToAdd);
            $tahun = date('Y');
            $bulan = date('m');

            if($objDateTime->format("m") > $bulan) {
                $sisathn = $objDateTime->format("Y") - $tahun;
                $sisabln = $objDateTime->format("m") - $bulan + 1;
                $sisajbtn = "$sisathn Tahun $sisabln bulan";

            } else {
                $sisathn = $objDateTime->format("Y") - $tahun - 1;
                $sisabln = $objDateTime->format("m") - $bulan + 13;
                if($sisabln >= 12) {
                    $sisathn = $objDateTime->format("Y") - $tahun;
                    $sisabln = 0;
                    $sisajbtn = "$sisathn Tahun $sisabln bulan";
                } else {
                    $sisajbtn = "$sisathn Tahun $sisabln bulan";
                }
            }
            $tglpensiun = $objDateTime->format('Y-m') . "-01";

        } else {
            $timeToAdd = "+ 60 years 1 months";
            // $objDateTime = DateTime::createFromFormat("Y-m-d",$tglahir);
            $objDateTime = $tglahir;
            
            // dd($objDateTime);
           
            $objDateTime->modify($timeToAdd);
            $tahun = date('Y');
            $bulan = date('m');

            if($objDateTime->format("m") > $bulan) {
                $sisathn = $objDateTime->format("Y") - $tahun;
                $sisabln = $objDateTime->format("m") - $bulan + 1;
                $sisajbtn = "$sisathn Tahun $sisabln bulan";

            } else {
                $sisathn = $objDateTime->format("Y") - $tahun - 1;
                $sisabln = $objDateTime->format("m") - $bulan + 13;
                if($sisabln >= 12) {
                    $sisathn = $objDateTime->format("Y") - $tahun;
                    $sisabln = 0;
                    $sisajbtn = "$sisathn Tahun $sisabln bulan";
                } else {
                    $sisajbtn = "$sisathn Tahun $sisabln bulan";
                }
            }
            $tglpensiun = $objDateTime->format('Y-m') . "-01";
        }
        
        $peg->sisa_jbtn = $sisajbtn;
        $peg->tgl_pensiun = $tglpensiun;
        $peg->tgl_lahir = DateExcel::excelToDateTimeObject($row['tgl_lahir']);
        // dd($tglpensiun);
        $peg->save();

        if(isset($row['tgl_berkala'])) {
            $row_berkala = DateExcel::excelToDateTimeObject($row['tgl_berkala']);
            $tglberkala = $row_berkala;
            $tglberkala->modify('+ 2 years');

            $data_berkala = new Berkala;
            $data_berkala->id_peg = $peg->id;
            $data_berkala->berkala_sblm = DateExcel::excelToDateTimeObject($row['tgl_berkala']);;
            $data_berkala->berkala_next = $tglberkala;
            $data_berkala->save();
        } else {
            return redirect('/list');
        }
        
    }
 
        
}