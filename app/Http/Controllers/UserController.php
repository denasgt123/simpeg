<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;

class UserController extends Controller
{
    public function index()
    {
        $user = \App\Models\User::all();

        return view('tm.user', [
            'user' => $user
        ]);
    }

    public function tambah(Request $request)
    {
        $usr = new \App\Models\User;

        $this->validate($request, [
            'name' => 'required | string |min:3|max:50',
            'email' => 'required | string | email',
            'password' => 'required | string | min:6 |confirmed'
        ]);

        DB::table('users')->insert([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'level' => $request->level,
        ]);

        Alert::success('Sukses Tambah', 'User baru berhasil ditambahkan');
        return redirect("/user/tambah");
    }


    public function hapus($id)
    {
        // menghapus data pegawai berdasarkan id yang dipilih
        DB::table('users')->where('id', $id)->delete();

        Alert::success('Sukses Hapus', 'User berhasil dihapus');

        // alihkan halaman ke halaman pegawai
        return redirect("/user/tambah");
    }
}
