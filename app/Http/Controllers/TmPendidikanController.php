<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;
use Auth;

class TmPendidikanController extends Controller
{
    public function index()
    {
        $pdd = \App\Models\Pendidikan::all();
        return view('tm.pendidikan', [
            'pdd' => $pdd
        ]);
    }

    public function tambah(Request $request)
    {
        $pdd = new \App\Models\Pendidikan;
        $pdd->pendidikan = $request->pendidikan;
        $pdd->save();

        Alert::success('Sukses Tambah', 'Data berhasil ditambahkan');
        return redirect("/pegawai/tmpendidikan/tambah");
    }


    public function hapus($id)
    {
        if(Auth::user()->level == 'admin') {
            // menghapus data pegawai berdasarkan id yang dipilih
            DB::table('tm_pendidikan')->where('kode_pdd', $id)->delete();

            Alert::success('Sukses Hapus', 'Data berhasil dihapus');

            // alihkan halaman ke halaman pegawai
            return redirect("/pegawai/tmpendidikan/tambah");

        } else {
            Alert::error('Hapus gagal', 'Anda Bukan Admin!');
            return redirect("/pegawai/tmpendidikan/tambah");
        }
        
    }
}
