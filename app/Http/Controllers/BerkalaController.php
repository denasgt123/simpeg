<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;
use \App\Models\Berkala;
use DateTime;
use PDF;
use Illuminate\Support\Carbon;

class BerkalaController extends Controller 
{
    public function index()
    {
        $tambah = "+ 2 months";
        $bulan = date('Y-m');
        $bln = DateTime::createFromFormat("Y-m",$bulan);
        $duabulan = DateTime::createFromFormat("Y-m",$bulan);
        $duabulan->modify($tambah);
        
        $berkalas = Berkala::join('pegawai', 'berkala.id_berkala', '=', 'pegawai.id_peg')->get(['pegawai.nip', 'pegawai.nama', 'berkala.*']);
        $berkala = Berkala::join('pegawai', 'berkala.id_berkala', '=', 'pegawai.id_peg')->whereBetween('berkala_next',[$bln, $duabulan])->get(['pegawai.*', 'berkala.*']);
        //  dd($pegawai);
        
        DB::table('pegawai')->where('tgl_pensiun','<=',Carbon::now())->delete();
        return view('berkala.index', compact('berkalas', 'berkala'));

    }


    public function edit($id)
    {

        $berkala = Berkala::where('id_berkala', $id)->first();
        $timeToAdd2 = "+ 40 years";
        $tgl_berkala = $berkala->berkala_next;
        
        $brkl = DateTime::createFromFormat("Y-m-d",$tgl_berkala);
        $tglberka = DateTime::createFromFormat("Y-m-d",$tgl_berkala);
        $tglberka->modify($timeToAdd2);

        $begin = $brkl;
        $end = $tglberka;
        $result = array();
    
        for($i = $begin; $i <= $end; $i->modify('+2 year')){
            $result[] = $i->format("Y-m-d");
        }

        // menambah berkalad berdasarkan data pegawai berdasarkan id yang dipilih
        DB::table('berkala')->where('id_berkala', $id)->update([
            'berkala_sblm' => $result[0],
            'berkala_next' => $result[1],
        ]);
        Alert::success('Sukses', 'Berkala Berhasil Diupdate');
        // alihkan halaman ke halaman pegawai
        return redirect('/berkala');
    }
    public function cetak_berkala(){
        $tambah = "+ 2 months";
        $bulan = date('Y-m');
        $bln = DateTime::createFromFormat("Y-m",$bulan);
        $duabulan = DateTime::createFromFormat("Y-m",$bulan);
        $duabulan->modify($tambah);
        
        $berkalas = Berkala::join('pegawai', 'berkala.id_berkala', '=', 'pegawai.id_peg')->whereBetween('berkala_next',[$bln, $duabulan])->get(['pegawai.*', 'berkala.*']);
        // dd($berkalas->nama);
        $pdf = PDF::loadview('berkala.berkala_pdf', compact('berkalas'));
        return $pdf->stream();
    }
}
?>