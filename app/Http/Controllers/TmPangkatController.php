<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;
use Auth;

class TmPangkatController extends Controller
{
    public function index()
    {
        $pang = \App\Models\Pangkat::all();
        return view('tm.pangkat', [
            'pang' => $pang
        ]);
    }

    public function tambah(Request $request)
    {
        $agm = new \App\Models\Pangkat;
        $agm->pangkat = $request->pangkat;
        $agm->save();

        Alert::success('Sukses Tambah', 'Data berhasil ditambahkan');
        return redirect("/pegawai/tmpangkat/tambah");
    }


    public function hapus($id)
    {
        if(Auth::user()->level == 'admin') {
            // menghapus data pegawai berdasarkan id yang dipilih
            DB::table('tm_pangkat')->where('kode_pang', $id)->delete();

            Alert::success('Sukses Hapus', 'Data berhasil dihapus');

            // alihkan halaman ke halaman pegawai
            return redirect("/pegawai/tmpangkat/tambah");
        } else {
            Alert::error('Hapus Gagal', 'Anda Bukan Admin!');
            return redirect("/pegawai/tmpangkat/tambah");
        }
    }
}
