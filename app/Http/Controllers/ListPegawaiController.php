<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;
use DateTime;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use \App\Models\ListPegawai;
use \App\Models\Berkala;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ListPegawaiImport;
use App\Http\Controllers\Controller;
use App\Models\Golongan;
use PDF;

class ListPegawaiController extends Controller
{

    public function beranda()
    {
        $pegawai = \App\Models\ListPegawai::all();
        $total_pegawai = \App\Models\ListPegawai::all()->count();
        $total_user = \App\Models\User::all()->count();
        $lk = \App\Models\ListPegawai::where('jns_kelamin', 'L')->count();
        $pr = \App\Models\ListPegawai::where('jns_kelamin', 'P')->count();
        
        return view('pegawai.index', [
            'pegawai' => $pegawai,
            'total_pegawai' => $total_pegawai,
            'total_user' => $total_user,
            'lk' => $lk,
            'pr' => $pr
        ]);
        // return dd($peg);
        // return response()->json(['data' => $pegawai]);
    }

    public function index()
    {
        $pegawai = ListPegawai::all();
        $user = \App\Models\User::all();
        $agama = \App\Models\Agama::all();
        $pendidikan = \App\Models\Pendidikan::all();
        $gol = \App\Models\Golongan::all();
        $berkala = \App\Models\Berkala::all();
        
        DB::table('pegawai')->where('tgl_pensiun','<=',Carbon::now())->delete();

        return view('pegawai.list', [
            'pegawai' => $pegawai,
            'user' => $user,
            'agama' => $agama,
            'pendidikan' => $pendidikan,
            'gol' => $gol,
            'berkala' => $berkala
        ]);

    }

    public function tambah(Request $request)
    {
        $this->validate($request, [
            'foto' => '',
            'nip' => '',
            'nama' => '',
            'email' => '',
            'kelamin' => '',
            'sts_pegawai' => '',
            't_lahir' => '',
            'tgl_lahir' => '',
            'tgl_berkala' => '',
            'tgl_mutasi' => '',
            'no_telp' => '',
            //tab 2
            'niplama' => '', 
            'agama' => '',
            'pendidikan' => '',
            'namapendidikan' => '',
            'namajabatan' => '',
            'sttb' => '',
             //tab 3
            'gol' => '',
            'tmt' => '',
            'pegawai' => '',
            'sts_pernikahan' => '',

        ]);
        $nip = $request->nip;

        // //Pensiun
        $tglahir = $request->tgl_lahir;
        $status = Str::lower($request->sts_pegawai);
        if($status == 'pegawai') {
            $timeToAdd = "+ 58 years 1 months";
            $objDateTime = DateTime::createFromFormat("Y-m-d",$tglahir);
            $objDateTime->modify($timeToAdd);
            $lahir = date('d-m-Y', strtotime($tglahir));
            $tahun = date('Y');
            $bulan = date('m');

            if($objDateTime->format("m") > $bulan) {
                $sisathn = $objDateTime->format("Y") - $tahun;
                $sisabln = $objDateTime->format("m") - $bulan + 1;
                $sisajbtn = "$sisathn Tahun $sisabln bulan";

            } else {
                $sisathn = $objDateTime->format("Y") - $tahun - 1;
                $sisabln = $objDateTime->format("m") - $bulan + 13;
                if($sisabln >= 12) {
                    $sisathn = $objDateTime->format("Y") - $tahun;
                    $sisabln = 0;
                    $sisajbtn = "$sisathn Tahun $sisabln bulan";
                } else {
                    $sisajbtn = "$sisathn Tahun $sisabln bulan";
                }
            }
            $tglpensiun = $objDateTime->format('Y-m') . "-01";

        } else {
            $timeToAdd = "+ 60 years 1 months";
            $objDateTime = DateTime::createFromFormat("Y-m-d",$tglahir);
            $objDateTime->modify($timeToAdd);
            $lahir = date('d-m-Y', strtotime($tglahir));
            $tahun = date('Y');
            $bulan = date('m');

            if($objDateTime->format("m") > $bulan) {
                $sisathn = $objDateTime->format("Y") - $tahun;
                $sisabln = $objDateTime->format("m") - $bulan + 1;
                $sisajbtn = "$sisathn Tahun $sisabln bulan";

            } else {
                $sisathn = $objDateTime->format("Y") - $tahun - 1;
                $sisabln = $objDateTime->format("m") - $bulan + 13;
                if($sisabln >= 12) {
                    $sisathn = $objDateTime->format("Y") - $tahun;
                    $sisabln = 0;
                    $sisajbtn = "$sisathn Tahun $sisabln bulan";
                } else {
                    $sisajbtn = "$sisathn Tahun $sisabln bulan";
                }
            }
            $tglpensiun = $objDateTime->format('Y-m') . "-01";
        }
         // insert data ke table pegawai
         $data_peg = new ListPegawai;
         $data_peg->nip = $request->nip;
         $data_peg->nama = $request->nama;
         $data_peg->email = $request->email;
         $data_peg->t_lahir = $request->t_lahir;
         $data_peg->tgl_lahir = $request->tgl_lahir;
         $data_peg->tgl_mutasi = $request->tgl_mutasi;
         $data_peg->sts_pegawai = $request->sts_pegawai;
         $data_peg->no_telp = $request->no_telp;
         $data_peg->jns_kelamin = $request->kelamin;
         $data_peg->nip_lama = $request->nip_lama;
         $data_peg->kode_agama = $request->agama;
         $data_peg->kode_pdd = $request->pendidikan;
         $data_peg->kode_gol = $request->gol;
         $data_peg->tmt = $request->tmt;
         $data_peg->nama_pendidikan = $request->namapendidikan;
         $data_peg->nama_jabatan = $request->namajabatan;
         $data_peg->tahun_sttb = $request->sttb;
         $data_peg->sts_marital = $request->sts_pernikahan;
         $data_peg->tgl_pensiun = $tglpensiun;
         $data_peg->sisa_jbtn = $sisajbtn;
         $data_peg->save();

        //Berkala
        if(isset($request->tgl_berkala)){
            $tglberkala = $request->tgl_berkala;
            $timeToAdd2 = "+ 40 years";
            $brkl = DateTime::createFromFormat("Y-m-d",$tglberkala);
            $tglberka = DateTime::createFromFormat("Y-m-d",$tglberkala);
            $tglberka->modify($timeToAdd2);

            $begin = $brkl;
            $end = $tglberka;
            $result = array();
        
            for($i = $begin; $i <= $end; $i->modify('+2 year')){
                $result[] = $i->format("Y-m-d");
            }

            // insert data ke table Berkala
            $data_berkala = new Berkala;
            $data_berkala->id_peg = $data_peg->id;
            $data_berkala->berkala_sblm = $result[0];
            $data_berkala->berkala_next = $result[1];
            $data_berkala->save();

            Alert::success('Sukses Tambah', 'Data berhasil ditambahkan');
            // alihkan halaman ke halaman pegawai
            return redirect('/list');
        } else {
            Alert::success('Sukses Tambah', 'Data berhasil ditambahkan');
            // alihkan halaman ke halaman pegawai
            return redirect('/list');
        }
    }

    public function hapus($id)
    {
        // menghapus data pegawai berdasarkan id yang dipilih
        DB::table('pegawai')->where('id_peg', $id)->delete();

        Alert::success('Sukses Hapus', 'Data berhasil Dihapus');
        // alihkan halaman ke halaman pegawai
        return redirect('/list');
    }


    public function edit($id, Request $request)
    {
        $this->validate($request, [
            'foto' => '',
            'nip' => '',
            'nama' => '',
            'email' => '',
            't_lahir' => '',
            'tgl_lahir' => '',
            'tgl_berkala' => '',
            'tgl_mutasi' => '',
            'no_telp' => '',
            'niplama' => '', //tab 2
            'pendidikan' => '',
            'namapendidikan' => '',
            'namajabatan' => '',
            'sttb' => '',
            'gol' => '',
            'tmt' => '',
            'agama' => '',
            'sts_pernikahan' => '',

        ]);

        $nip = $request->nip;

        // insert data ke table pegawai
        DB::table('pegawai')->where('id_peg', $id)->update([
            'nip' => $request->nip,
            'nama' => $request->nama,
            't_lahir' => $request->t_lahir,
            'tgl_lahir' => $request->tgl_lahir,
            'tgl_berkala' => $request->tgl_berkala,
            'tgl_mutasi' => $request->tgl_mutasi,
            'no_telp' => $request->no_telp,
            'nip_lama' => $request->niplama,
            'email' => $request->email,
            'kode_pdd' => $request->pendidikan,
            'nama_pendidikan' => $request->namapendidikan,
            'nama_jabatan' => $request->namajabatan,
            'tahun_sttb' => $request->sttb,
            'kode_gol' => $request->gol,
            'tmt' => $request->tmt,
            'kode_agama' => $request->agama,
            'sts_marital' => $request->sts_pernikahan,

        ]);
        Alert::success('Sukses Edit', 'Data berhasil di-Edit');
        // alihkan halaman ke halaman pegawai
        return redirect('/list');
    }

    public function profile($id)
    {
        // menghapus data pegawai berdasarkan id yang dipilih
        $pegawai = \App\Models\ListPegawai::where('id_peg', $id)->with([])->first();
        $pendidikans = \App\Models\Pendidikan::all();
        $gol = \App\Models\Golongan::all();
        //  alihkan halaman ke halaman pegawai
        //  return dd($pegawai);
        return view('pegawai.profile', [
            'pegawai' => $pegawai,
            'pendidikans' => $pendidikans,
            // 'edit' => $edit,
            'gol' => $gol,
        ]);
        //return dd($pegawai);
    }

    public function import_excel(Request $request) 
    {
	// validasi
	$this->validate($request, [
		'file' => 'required|mimes:csv,xls,xlsx'
	]);
 
	// menangkap file excel
	$file = $request->file('file');
 
	// membuat nama file unik
	$nama_file = rand().$file->getClientOriginalName();
 
	// upload ke folder file_siswa di dalam folder public
	$file->move('public',$nama_file);
 
	// import data
	Excel::import(new ListPegawaiImport, public_path('/public/'.$nama_file));
 
	// notifikasi dengan session
	Alert::success('Sukses', 'Data berhasil diimport');
 
	// alihkan halaman kembali
	return redirect('/list');
    }

    public function cetak_pdf($id){
        $pegawai = ListPegawai::where('id_peg', $id)->with([])->first();
        $pendidikans = \App\Models\Pendidikan::all();
        
        $berkalas = Berkala::join('pegawai', 'berkala.id_berkala', '=', 'pegawai.id_peg')->where('berkala.id_peg', $id)->first(['pegawai.*', 'berkala.*']);

        // dd($pegawai->pendidikan);
        $pdf = PDF::loadview('pegawai/list_pdf', compact('pegawai', 'berkalas'));
        return $pdf->stream();
    }

    public function cetak_gol(Request $request){
        $this->validate($request, [
            'gol' => '',

        ]);

        $cekGol = ListPegawai::where('kode_gol', $request->gol)->first();
        if (isset($cekGol)) {
            // dd($request->gol);
            $golongan = ListPegawai::where('kode_gol', $request->gol)->get();
            $gol = ListPegawai::where('kode_gol', $request->gol)->first();
            // dd($golongan->golongan->golongan);
            $pdf = PDF::loadview('tm/gol_pdf', compact('golongan', 'gol'));
            return $pdf->stream();
        }else{
            Alert::error('ERROR', 'Data Tidak Ditemukan!');
            return redirect()->back();
        }
    }
}
