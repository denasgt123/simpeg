<?php

namespace App\Http\Controllers;
use \App\Models\ListPegawai;
use App\Http\Controllers\Controller;
use PDF;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use DateTime;

class PensiunController extends Controller {
    public function index(){
        $pegawai = \App\Models\ListPegawai::orderBy('tgl_pensiun', 'asc')->get();
        DB::table('pegawai')->where('tgl_pensiun','<=',Carbon::now())->delete();
        // dd($pegawai);
        return view('pensiun.index', [
            'pegawai' => $pegawai,
        ]);
    }

    public function cetak_pensiun(){
        $tambah = "+ 2 years";
        $tahun = date('Y');
        $duatahun = DateTime::createFromFormat("Y",$tahun);
        $duatahun->modify($tambah);
        
        $pegawaipensiun = ListPegawai::where('tgl_pensiun', '<=' ,$duatahun)->get();
        $pdf = PDF::loadview('pensiun.pensiun_pdf', compact('pegawaipensiun'));
        return $pdf->stream();
    }
}
?>