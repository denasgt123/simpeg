<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;
use  App\Models\User;
use Auth;
class TmAgamaController extends Controller
{
    public function index()
    {
        $agama = \App\Models\Agama::all();
        $user = \App\Models\User::all();
        return view('tm.agama', [
            'agama' => $agama,
            'user' => $user
        ]);
    }

    public function tambah(Request $request)
    {
        $agm = new \App\Models\Agama;
        $agm->agama = $request->agama;
        $agm->save();

        Alert::success('Sukses Tambah', 'Data berhasil ditambahkan');
        return redirect("/pegawai/tmagama/tambah");
    }


    public function hapus($id)
    {
        if(Auth::user()->level == 'admin') {
            DB::table('tm_agama')->where('kode_agama', $id)->delete();

            Alert::success('Sukses Hapus', 'Data berhasil dihapus');

            // alihkan halaman ke halaman pegawai
            return redirect("/pegawai/tmagama/tambah");
        } else {
            Alert::error('Hapus Gagal', 'Anda Bukan Admin!');
            return redirect("/pegawai/tmagama/tambah");
        }
    }
}
