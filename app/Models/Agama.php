<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Agama extends Model
{
    use HasFactory;
    protected $table = 'tm_agama';

    public function Pegawai()
    {
        return $this->belongsTo(Pegawai::class);
    }
}
