<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ListPegawai;

class Berkala extends Model
{
    use HasFactory;
    protected $table = 'berkala';

    public function ListPegawai()
    {
        return $this->hasMany(ListPegawai::class, 'id_peg', 'id_berkala');
    }
}
