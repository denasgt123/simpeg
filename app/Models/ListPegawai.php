<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Berkala;

class ListPegawai extends Model
{
    use HasFactory;
    protected $table = 'pegawai';

    public function berkala()
    {
        return $this->belongsTo(Berkala::class);
    }

    public function agama()
    {
        return $this->hasOne(Agama::class, 'kode_agama', 'kode_agama');
    }

    public function golongan()
    {
        return $this->hasOne(Golongan::class, 'kode_gol', 'kode_gol');
    }

    public function pensiun()
    {
        return $this->hasOne(Pensiun::class, 'id_pensiun', 'id_pensiun');
    }

    public function pendidikan()
    {
        return $this->hasMany(Pendidikan::class, 'kode_pdd', 'kode_pdd');
    }
    
}
