<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ListPegawai;

class Golongan extends Model
{
    use HasFactory;
    protected $table = 'tm_golongan';

    public function ListPegawai()
    {
        return $this->belongsTo(ListPegawai::class);
    }
}
