    <?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;

    class CreateBerkalaTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('berkala', function (Blueprint $table) {
                $table->increments('id_berkala');
                $table->unsignedInteger('id_peg')->nullable();
                $table->date('berkala_sblm')->nullable();
                $table->date('berkala_next')->nullable();
                $table->string('gaji')->nullable();
                $table->string('status')->nullable();
                $table->timestamps();

                $table->foreign('id_peg')->references('id_peg')->on('pegawai')
                    ->onDelete('set null')->onUpdate('cascade');
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('berkala');
        }
    }
