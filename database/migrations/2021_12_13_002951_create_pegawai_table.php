<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePegawaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pegawai', function (Blueprint $table) {
            $table->increments('id_peg');
            $table->string('nip')->nullable();;
            $table->string('nama')->nullable();
            $table->string('nip_lama')->nullable();
            $table->string('t_lahir')->nullable();
            $table->string('no_telp')->nullable();
            $table->string('sisa_jbtn')->nullable();
            $table->string('foto')->nullable();;
            $table->string('nama_pendidikan')->nullable();
            $table->string('nama_jabatan')->nullable();
            $table->string('tahun_sttb')->nullable();
            $table->string('email')->nullable();
            $table->date('tgl_pensiun')->nullable();
            $table->date('tgl_lahir')->nullable();
            $table->date('tmt')->nullable();
            $table->date('tgl_mutasi')->nullable();
            $table->string('sts_pegawai')->nullable();
            $table->enum('jns_kelamin', ['L', 'P']);
            $table->enum('sts_marital', ['Menikah', 'Belum menikah'])->nullable();
            $table->unsignedInteger('kode_agama')->nullable();
            $table->unsignedInteger('kode_pdd')->nullable();
            $table->unsignedInteger('id_user')->nullable();;
            $table->unsignedInteger('kode_gol')->nullable();


            $table->foreign('kode_agama')->references('kode_agama')->on('tm_agama')
                ->onDelete('set null')->onUpdate('cascade');
            $table->foreign('kode_pdd')->references('kode_pdd')->on('tm_pendidikan')
                ->onDelete('set null')->onUpdate('cascade');
            $table->foreign('kode_gol')->references('kode_gol')->on('tm_golongan')
                ->onDelete('set null')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pegawai');
    }
}
