<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ListPegawai;
use App\Models\User;
use App\Models\Agama;
use App\Models\Pangkat;
use App\Models\Golongan;
use App\Models\Pendidikan;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        // Seed User
        User::create([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'level' => 'admin',
            'password' => bcrypt('admin')
        ]);
        
        User::create([
            'name' => 'ferry',
            'email' => 'ferry@gmail.com',
            'level' => 'user',
            'password' => bcrypt('ferry')
        ]);
        
        // Seed Agama
        Agama::create([
            'agama' => 'Islam',
        ]);
        
        Agama::create([
            'agama' => 'Kristen',
        ]);
        
        Agama::create([
            'agama' => 'Hindu',
        ]);
        
        Agama::create([
            'agama' => 'Buddha',
        ]);
        
        // Seed Golongan
        Golongan::create([
            'golongan' => 'IX',
        ]);
        
        Golongan::create([
            'golongan' => 'IV / c',
            'pangkat' => 'Pembina Utama Muda',
        ]);
        
        Golongan::create([
            'golongan' => 'IV / b',
            'pangkat' => 'Pembina TK. 1',
        ]);

        Golongan::create([
            'golongan' => 'IV / a',
            'pangkat' => 'Pembina',
        ]);

        Golongan::create([
            'golongan' => 'III / d',
            'pangkat' => 'Penata Tk. 1',
        ]);

        Golongan::create([
            'golongan' => 'III / c',
            'pangkat' => 'Penata',
        ]);
        
        Golongan::create([
            'golongan' => 'III / b',
            'pangkat' => 'Penata Muda Tk. 1',
        ]);
        
        Golongan::create([
            'golongan' => 'III / a',
            'pangkat' => 'Penata Muda',
        ]);
        
        Golongan::create([
            'golongan' => 'II / b',
            'pangkat' => 'Pengatur Muda Tk. 1',
        ]);

        Golongan::create([
            'golongan' => 'GTT',
        ]);

        Golongan::create([
            'golongan' => 'PTT',
        ]);

        // Seed Pendidikan
        Pendidikan::create([
            'pendidikan' => 'S2',
        ]);
        
        Pendidikan::create([
            'pendidikan' => 'S1',
        ]);
        
        Pendidikan::create([
            'pendidikan' => 'D4',
        ]);
        
        Pendidikan::create([
            'pendidikan' => 'D3',
        ]);

        Pendidikan::create([
            'pendidikan' => 'SMA',
        ]);

        Pendidikan::create([
            'pendidikan' => 'SMK',
        ]);

        Pendidikan::create([
            'pendidikan' => 'MA',
        ]);

        Pendidikan::create([
            'pendidikan' => 'SD',
        ]);

    }
}
